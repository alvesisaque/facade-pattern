import sys
sys.path.insert(1, '../../src')

from PreProcessor import PreProcessor
import pandas as pd
import numpy as np
import en_core_web_sm
import operator
from functools import reduce

nlp = en_core_web_sm.load()

def test_tokenize_single_text_remove_stopwords():
    text_dirty = 'Bioaerosol levels were clearly diminished when using the HSCAH compared with the air turbine.'
    _preProcessor = PreProcessor(text_dirty)
    text = _preProcessor.tokenize_single_text(text_dirty, include_punctuation=True, include_only_ascii=False)
    text_str = [str(tokens) for tokens in text]
    text_pre_porecessed = " ".join(text_str)
    text_expected = 'Bioaerosol levels clearly diminished HSCAH compared air turbine .'
    assert text_pre_porecessed == text_expected


def test_tokenize_single_text_remove_punctuation():
    text_dirty = "But those who were vaccinated had a 60%% reduced risk of being hospitalized with the delta variant than unvaccinated people who caught delta. Related: Coronavirus variants: Here's how the SARS-CoV-2 mutants stack up Another study from Canada, posted to the preprint website medRxiv on July 14, found that people infected with the delta variant were twice as likely to be hospitalized, and twice as likely to die, as those infected with a coronavirus strain that wasn't a ""variant of concern"" (i.e., not infected with alpha, beta, gamma or delta variants)."
    _preProcessor = PreProcessor(text_dirty)
    text = _preProcessor.tokenize_single_text(text_dirty, include_stopwords=True, include_punctuation=False, include_like_num=True, include_only_ascii=False, include_pos=[])
    text_str = [str(tokens) for tokens in text]
    text_pre_porecessed = " ".join(text_str)
    text_expected = "But those who were vaccinated had a 60%% reduced risk of being hospitalized with the delta variant than unvaccinated people who caught delta Related Coronavirus variants Here 's how the SARS CoV-2 mutants stack up Another study from Canada posted to the preprint website medRxiv on July 14 found that people infected with the delta variant were twice as likely to be hospitalized and twice as likely to die as those infected with a coronavirus strain that was n't a variant of concern i.e. not infected with alpha beta gamma or delta variants"
    assert text_pre_porecessed == text_expected


def test_tokenize_single_text_remove_number():
    text_with_number = '1234567890'
    _preProcessor = PreProcessor(text_with_number)
    text_without_number = _preProcessor.tokenize_single_text(text_with_number)
    text_expected = []
    
    assert text_without_number == text_expected


def test_tokenize_single_text_let_number():
    text_with_number = '1234567890'
    _preProcessor = PreProcessor(text_with_number)
    text_without_number = _preProcessor.tokenize_single_text(text_with_number, include_like_num = True)
    text_expected = '1234567890'
    
    assert str(text_without_number[0]) == text_expected


def test_tokenize_single_text_remove_ascii():
    text_dirty = "코로나19로 한 공간에 많은 사람이 모이는 것 자체가 법에 저촉되는 일이 됨에 따라 비대면 기술에 대한 수요가 폭증했는데요."
    _preProcessor = PreProcessor(text_dirty)
    text = _preProcessor.tokenize_single_text(text_dirty, include_stopwords=True, include_punctuation=True, include_like_num=True, include_only_ascii=True, include_pos=[])
    text_str = [str(tokens) for tokens in text]
    text_pre_porecessed = " ".join(text_str)
    text_expected = "."
 
    assert text_pre_porecessed == text_expected


def test_tokenize_single_text_pos_verb():
    text_dirty = 'Bioaerosol levels were clearly diminished when using the HSCAH compared with the air turbine.'
    _preProcessor = PreProcessor(text_dirty)
    text = _preProcessor.tokenize_single_text(text_dirty, include_stopwords=True, include_punctuation=True, include_like_num=True, include_only_ascii=True, include_pos=['VERB'])
    text_str = [str(tokens) for tokens in text]
    text_pre_porecessed = " ".join(text_str)
    text_expected = 'diminished using compared'
    assert text_pre_porecessed == text_expected


def test_tokenize_single_text_pos_noun():
    text_dirty = 'Bioaerosol levels were clearly diminished when using the HSCAH compared with the air turbine.'
    _preProcessor = PreProcessor(text_dirty)
    text = _preProcessor.tokenize_single_text(text_dirty, include_stopwords=True, include_punctuation=True, include_like_num=True, include_only_ascii=True, include_pos=['NOUN'])
    text_str = [str(tokens) for tokens in text]
    text_pre_porecessed = " ".join(text_str)
    print(text_pre_porecessed)
    text_expected = 'levels air turbine'
    assert text_pre_porecessed == text_expected
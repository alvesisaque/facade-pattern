import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

class Tfidf:
    def __init__(self, list_of_tokens=pd.Series()):
        self.list_of_tokens = list_of_tokens
        self.ngram_min=1
        self.ngram_max=1
        self.number_of_tokens=10

    def get_tf_idf(self, list_of_tokens, ngram_min=1, ngram_max=1):
        vectorizer = TfidfVectorizer(preprocessor = ' '.join, ngram_range=(ngram_min,ngram_max))
        vectors = vectorizer.fit_transform(list_of_tokens)
        feature_names = vectorizer.get_feature_names()
        vectors_list = vectors.todense().tolist()
        return vectors_list, feature_names

    def get_tf_idf_matrix(self, vectors_list, feature_names):
        return pd.DataFrame(vectors_list, columns=feature_names)

    def get_most_significant_tokens(self, vectors_list, feature_names, number_of_tokens=10):
        most_significant_tokens_matrix = []
        for vector in vectors_list:
            tokens = []
            for _, feature in sorted(zip(vector, feature_names), reverse=True)[:number_of_tokens]:
                tokens.append(feature)
            most_significant_tokens_matrix.append(tokens)
        return most_significant_tokens_matrix

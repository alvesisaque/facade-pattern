from utils import remove_special_characters, clean_broke_lines, default_data_filename
import pandas as pd
from sklearn.model_selection import train_test_split

class Splitter:
    def __init__(self, dataframe=pd.DataFrame()):
        self.dataframe = dataframe
    
    def data_split(self, dataframe):
        
        X_train, X_teste = train_test_split(dataframe, test_size = .3, stratify = dataframe[['crawler']], random_state=1)
        X_teste.to_csv('../data/teste.csv', index=False)
        X_train.to_csv('../data/train.csv', index=False)
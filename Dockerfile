FROM ubuntu:20.04

RUN apt update -y
RUN apt install -y python3-pip libpq-dev python-dev

COPY requirements.txt requirements.txt
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN pip3 install jupyterlab
RUN python3 -m spacy download en_core_web_sm


COPY data /projeto/data
COPY notebooks /projeto/notebooks
COPY src /projeto/src
COPY test /projeto/test
WORKDIR /projeto
CMD jupyter-lab --ip='*' --port=8888 --allow-root --NotebookApp.token='' --NotebookApp.password=''